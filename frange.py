# class frange: # noqa
#
#     def __init__(self, *args):
#         if len(args) == 1:
#             start, end, step = 0, args[0], 1
#         elif len(args) == 2:
#             start, end, step = args[0], args[1], 1
#         elif len(args) == 3:
#             start, end, step = args[0], args[1], args[2]
#
#         if step == 0:
#             raise ValueError('frange() arg 3 must not be zero')
#
#         self.end = end
#         self.start = start
#         self.step = step
#         self._pointer = 0
#         self._execute = self._execute()
#
#     def __next__(self):
#         if self._pointer == len(self._execute):
#             raise StopIteration
#         result = self._execute[self._pointer]
#         self._pointer += 1
#         return result
#
#     def _execute(self):
#         result = []
#         result.append(self.start)
#         num = self.start
#         if self.start < self.end:
#             while num < self.end:
#                 num += self.step
#                 result.append(num)
#             return result[:-1]
#         elif self.start > self.end:
#             while num > self.end:
#                 num += self.step
#                 result.append(num)
#             return result[:-1]
#         else:
#             return result[:-1]
#
#     def __iter__(self):
#         return self
#
#
# if __name__ == '__main__':
#
#     assert (list(frange(5)) == [0, 1, 2, 3, 4])
#     assert (list(frange(2, 5)) == [2, 3, 4])
#     assert (list(frange(2, 10, 2)) == [2, 4, 6, 8])
#     assert (list(frange(10, 2, -2)) == [10, 8, 6, 4])
#     assert (list(frange(1, 5)) == [1, 2, 3, 4])
#     assert (list(frange(0, 5)) == [0, 1, 2, 3, 4])
#     assert (list(frange(0, 0)) == [])
#     assert (list(frange(100, 0)) == [])
#     assert (list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
